import express from "express";

import "./database";

const app = express(); // criando um app (servidor)



/*
    Tipos de rota e definindo os tipos de rota
    GET -> Buscas
    POST -> Criação
    PUT -> Alteração (updates)
    DELETE -> Deletar
    PATCH -> Alterar uma informação específica, ex: Alterar a senha de um usuário
*/
app.get("/", (request, response) => {
    return response.json({
        message: "Olá NLW 05"
    });
});

app.post("/users", (request, response) => {
    return response.json({
        message: "Usuário salvo com sucesso!"
    });
});



app.listen(3333, () => console.log("Server is running on port 3333")); // iniciando servidor